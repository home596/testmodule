<?php

namespace dion\testmodule\migrations;

use yii\db\Migration;

/**
 * Class m231102_111215_add_params
 */
class m231102_111215_add_params extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('params', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'value' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('params');
    }

}
