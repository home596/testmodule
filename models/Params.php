<?php

namespace dion\testmodule\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

class Params extends ActiveRecord
{

    final public static function tableName()
    {
        return 'params';
    }

    public function rules()
    {
        //add some rules if need
        return [
            [['name', 'value'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Param name',
            'value' => 'Param value'
        ];
    }

    public function getProvider()
    {
        $provider = new ActiveDataProvider([
            'query' => self::find(),
        ]);
        return $provider;
    }

}
