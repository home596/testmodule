<?php

namespace dion\testmodule\controllers;

use dion\testmodule\models\Params;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `module` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new Params();
        $model->load(Yii::$app->request->get());
        $provider = $model->getProvider();
        return $this->render('index', compact('model', 'provider'));
    }

    public function actionCreate()
    {
        $model = new Params();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['default/index']);
        }
        return $this->render('create', compact('model'));

    }

    public function actionUpdate($id)
    {
        $model = Params::findOne(['id' => $id]);
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['default/index']);
        }
        return $this->render('update', compact('model'));

    }

    public function actionDelete($id)
    {
        $model = Params::findOne(['id' => $id]);
        if ($model) {
            $model->delete();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

}
