<?php

namespace dion\testmodule;


use dion\testmodule\models\Params;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;
use Yii;

class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'dion\testmodule\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->updateParams();
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        self::setAliases(['@' . str_replace('\\', '/', __NAMESPACE__) => __DIR__]);
        $this->controllerNamespace = __NAMESPACE__ . '\controllers';
        $this->addMigrationNamespace($app);
    }

    private function addMigrationNamespace(Application $app)
    {
        if (!isset($app->controllerMap['migrate'])) {
            $app->controllerMap['migrate'] = [
                'class' => \yii\console\controllers\MigrateController::class,
            ];
        }
        if (!isset($app->controllerMap['migrate']['migrationPath'])) {
            $app->controllerMap['migrate']['migrationPath'] = ['@app/migrations'];
        }
        $app->controllerMap['migrate']['migrationNamespaces'][] = __NAMESPACE__ . '\migrations';
    }

    public function updateParams()
    {
        $dbParams = ArrayHelper::map(Params::find()->all(), 'name', 'value');
        Yii::$app->params = array_merge($dbParams, Yii::$app->params);
    }


}
