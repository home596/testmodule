<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="br-section-wrapper">
    <?php $form = ActiveForm::begin([]); ?>

    <?= $form->field($model, 'name')->textInput(); ?>
    <?= $form->field($model, 'value')->textInput(); ?>

    <?php
    $btnName = $model->isNewRecord ? 'Сохранить' :'Обновить';
    echo Html::submitButton($btnName, ['class' => 'btn btn-primary btn-sm']);
    ?>

    <?php ActiveForm::end(); ?>
</div>
