<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @var \dion\testmodule\models\Params $model
 * @var $provider
 */

?>
<div class="br-section-wrapper mb-4">
    <?= Html::a('Add new param', ['create'], [
        'class' => 'btn btn-sm btn-outline-primary',
    ]) ?>
</div>
<div class="br-section-wrapper">
    <?= GridView::widget([
        'dataProvider' => $provider,
        'filterModel' => $model,
        'options' => [
            'tag' => 'div',
            'class' => 'bd bd-gray-300 rounded table-responsive',
        ],
        'tableOptions' => ['class' => 'table table-bordered table-striped mg-b-0'],
        'layout' => '{items}',
        'columns' => [
            [
                'header' => 'Name',
                'format' => 'text',
                'attribute' => 'name',
                'value' => $model->name,
                'filter' => false,
                'headerOptions' => [
                    'text-align: center;'
                ],
                'contentOptions' => [
                    'text-align: center;'
                ],
            ],
            [
                'header' => 'Value',
                'format' => 'text',
                'attribute' => 'value',
                'value' => $model->value,
                'filter' => false,
                'headerOptions' => [
                    'text-align: center;'
                ],
                'contentOptions' => [
                    'text-align: center;'
                ],
            ],
            [
                'class' => ActionColumn::class,
                'visibleButtons' => [
                    'update' => function () {
                        return true;
                    },
                    'delete' => function () {
                        return true;
                    },
                ],
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(Html::tag('i', '', ['class' => 'fa fa-pencil']), $url, [
                            'class' => 'btn btn-xs btn-primary',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'delete' => function ($url) {
                        return Html::a(Html::tag('i', '', ['class' => 'fa fa-trash']), $url, [
                            'class' => 'btn btn-xs btn-danger btn-danger-confirm',
                            'title' =>  'Удалить',
                        ]);
                    },
                ],
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>
